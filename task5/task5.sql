-- Task 1
SELECT `id`, UPPER(`name`) as `name` FROM `clients` LIMIT 10;

SELECT `id`, LOWER(`name`) as `name` FROM `clients` LIMIT 10;


-- Task 2
SELECT CONCAT(`name`, ' ', `lastname`) as `full_name`,
       CONCAT(SUBSTRING(`phone`, 1, 3), LPAD('', LENGTH(`phone`) - 4, '*'), SUBSTRING(`phone`, -1)) as `phone_number`
FROM `clients` LIMIT 10;


-- Task 3
SELECT CONCAT(SUBSTR(`name`, 1, 1), '. ', `lastname`) as `name`
FROM `clients`
WHERE LOCATE('tt', `lastname`) OR LOCATE('ss', `lastname`) OR LOCATE('ll', `lastname`)
LIMIT 10;


-- Task 4

SELECT `id`, `phone` FROM `clients`
WHERE LOCATE('586', `phone`) = 1
LIMIT 10;

SELECT `id`, `phone` FROM `clients`
WHERE LOCATE('657', `phone`)
LIMIT 10;

SELECT `id`, `phone` FROM `clients`
WHERE LOCATE('707', `phone`) = LENGTH(`phone`) - 2
LIMIT 10;

