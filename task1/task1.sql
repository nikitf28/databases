/* by Names*/
/* Robert */
SELECT `id`, `name`, `lastname`, `dbirth`, `gender`, `phone` FROM `clients` WHERE `name`='Robert' LIMIT 20;
/* Jeffrey */
SELECT `id`, `name`, `lastname`, `dbirth`, `gender`, `phone` FROM `clients` WHERE `name`='Jeffrey' LIMIT 20;
/* John */
SELECT `id`, `name`, `lastname`, `dbirth`, `gender`, `phone` FROM `clients` WHERE `name`='John' LIMIT 20;
/* Richard */
SELECT `id`, `name`, `lastname`, `dbirth`, `gender`, `phone` FROM `clients` WHERE `name`='Richard' LIMIT 20;
/* David */
SELECT `id`, `name`, `lastname`, `dbirth`, `gender`, `phone` FROM `clients` WHERE `name`='David' LIMIT 20;
/* Robert, Jeffrey, John, Richard, David */
SELECT `id`, `name`, `lastname`, `dbirth`, `gender`, `phone` FROM `clients` WHERE `name`='Robert' OR `name`='Jeffrey' OR `name`='John' OR `name`='Richard' OR `name`='David';


/* by Genders */
/* Female */
SELECT `id`, `name`, `lastname`, `dbirth`, `gender`, `phone` FROM `clients` WHERE `gender`='F';
/* Male */
SELECT `id`, `name`, `lastname`, `dbirth`, `gender`, `phone` FROM `clients` WHERE `gender`='M';



/* Name and Phone by Lastname */
/*Williams */
SELECT `name`, `phone` FROM `clients` WHERE `lastname`='Williams' LIMIT 20;
/* Jones */
SELECT `name`, `phone` FROM `clients` WHERE `lastname`='Jones' LIMIT 20;
/* Brown */
SELECT `name`, `phone` FROM `clients` WHERE `lastname`='Brown' LIMIT 20;
/* Miller */
SELECT `name`, `phone` FROM `clients` WHERE `lastname`='Miller' LIMIT 20;
/* Williams, Jones, Brown, Miller */
SELECT `name`, `phone` FROM `clients` WHERE `lastname`='Williams' OR `lastname`='Jones' OR `lastname`='Brown' OR `lastname`='Miller';


/* Dbirth and Gender by Name or Lastname */
/* Williams */
SELECT `dbirth`, `gender` FROM `clients` WHERE `lastname`='Williams' OR `name`='Williams' LIMIT 20;
/* Thomas */
SELECT `dbirth`, `gender` FROM `clients` WHERE `lastname`='Thomas' OR `name`='Thomas' LIMIT 20;
/* Clark */
SELECT `dbirth`, `gender` FROM `clients` WHERE `lastname`='Clark' OR `name`='Clark' LIMIT 20;
/* Joseph */
SELECT `dbirth`, `gender` FROM `clients` WHERE `lastname`='Joseph' OR `name`='Joseph' LIMIT 20;

/* Gender, Dbirth, Phone by Name and Lastname */
/* Tom Brown */
SELECT `gender`, `dbirth`, `phone` FROM `clients` WHERE `lastname`='Brown' AND `name`='Tom' LIMIT 20;
/* Janet Silberstein */
SELECT `gender`, `dbirth`, `phone` FROM `clients` WHERE `lastname`='Silberstein' AND `name`='Janet' LIMIT 20;
/* John Foster */
SELECT `gender`, `dbirth`, `phone` FROM `clients` WHERE `lastname`='Foster' AND `name`='John' LIMIT 20;