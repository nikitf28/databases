-- Task 1
SELECT s.lastname, s.firstname, su.name AS `subject`, GROUP_CONCAT(sm.mark SEPARATOR ' ') AS `marks`
FROM `students` AS `s`, `student_marks` AS `sm`, `subjects` AS `su`
WHERE s.id = sm.student_id AND su.id = sm.subject_id
GROUP BY s.id, su.id
ORDER BY s.lastname, s.firstname, su.name;


-- Task 2
SELECT s.lastname, s.firstname, su.name AS `subject`, AVG(sm.mark) AS `AVG mark`
FROM `students` AS `s`, `student_marks` AS `sm`, `subjects` AS `su`
WHERE s.id = sm.student_id AND su.id = sm.subject_id
GROUP BY s.id, su.id
ORDER BY s.lastname, s.firstname, su.name;

-- Task 3
SELECT s.lastname, s.firstname, sm.mark AS `mark`, COUNT(sm.mark) AS `amount`
FROM `students` AS `s`, `student_marks` AS `sm`
WHERE s.id = sm.student_id
GROUP BY s.id, sm.mark
ORDER BY s.lastname, s.firstname, sm.mark DESC;

-- Task 4
SELECT s.lastname, s.firstname, AVG(sm.mark) AS `AVG mark`
FROM `students` AS `s`, `student_marks` AS `sm`
WHERE s.id = sm.student_id
GROUP BY s.id
ORDER BY `AVG mark` DESC
LIMIT 1;

SELECT s.lastname, s.firstname, AVG(sm.mark) AS `AVG mark`
FROM `students` AS `s`, `student_marks` AS `sm`
WHERE s.id = sm.student_id
GROUP BY s.id
ORDER BY `AVG mark`
LIMIT 1;

-- Task 5
SELECT su.name, AVG(sm.mark) AS `AVG mark`
FROM `student_marks` AS `sm`, `subjects` AS `su`
WHERE su.id = sm.subject_id
GROUP BY su.id
ORDER BY `AVG mark` DESC
LIMIT 1;

-- Task 6
SELECT ss.date, GROUP_CONCAT(su.name SEPARATOR ' ') AS `subjects`
FROM `subjects` AS `su`, `subject_schedules` AS `ss`
WHERE su.id = ss.subject_id
GROUP BY ss.date
ORDER BY ss.date;