-- Task 1
CREATE TABLE `student_presents`(
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `subject_schedule_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
    `student_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    KEY `subject_schedule_id` (`subject_schedule_id`),
    KEY `student_id` (`student_id`)
);


-- Task 2: in file presents_generation.py

-- Task 3

SELECT ss.date, su.name, COUNT(*) AS `amount_of_students`
FROM `subject_schedules` AS `ss`
LEFT JOIN  `student_presents` AS `sp` ON sp.subject_schedule_id = ss.id
LEFT JOIN `subjects` AS `su` ON ss.subject_id = su.id
GROUP BY ss.id
ORDER BY ss.date, su.name;

SELECT s.firstname, s.lastname, su.name, COUNT(*) / (
    SELECT COUNT(*)
    FROM `student_presents`
    LEFT JOIN `subject_schedules` ON subject_schedules.id = student_presents.subject_schedule_id
    LEFT JOIN `subjects` ON subjects.id = subject_schedules.subject_id
    WHERE subjects.name = su.name
                                                         ) AS `presents_rate`
FROM `student_presents` AS `sp`
LEFT JOIN `subject_schedules` AS `ss` ON sp.subject_schedule_id = ss.id
LEFT JOIN `subjects` AS `su` ON ss.subject_id = su.id
LEFT JOIN `students` AS `s` ON sp.student_id = s.id
GROUP BY s.lastname, s.firstname, su.name
ORDER BY s.lastname, s.firstname, su.name;
