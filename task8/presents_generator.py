import random

import pymysql

con = pymysql.connect(host='localhost',
                      user='nikita',
                      password='super618',
                      database='homework')

lazyness = [0.2, 0.3, 0.5, 0.75, 0.95]

with con:
    for subject_schedule in range(1, 244):
        for student in range(1, 6):
            if random.random() < lazyness[student - 1]:
                cur = con.cursor()
                cur.execute("INSERT INTO `student_presents`(`subject_schedule_id`, `student_id`) "
                            "VALUES('%s', '%s') " % (subject_schedule, student))
                con.commit()
                print(student, subject_schedule)