-- Task 1

SELECT `id` FROM `clients` WHERE `name` = 'Gary' and `lastname` = 'Harrison';
-- 782, total: 1
UPDATE `clients` SET `dbirth` = '1930-10-21' WHERE `id` = 782 LIMIT 1;
-- Updated: 1

SELECT `id` FROM `clients` WHERE `name` = 'Michael' and `lastname` = 'Atwood';
-- 297, total: 1
UPDATE `clients` SET `dbirth` = '1934-04-12' WHERE `id` = 297 LIMIT 1;
-- Updated: 0

SELECT `id` FROM `clients` WHERE `name` = 'Amy' and `lastname` = 'Majors';
-- 240, total: 1
UPDATE `clients` SET `dbirth` = '1972-08-01' WHERE `id` = 240 LIMIT 1;
-- Updated: 1

SELECT `id` FROM `clients` WHERE `name` = 'Katherine' and `lastname` = 'Smith';
-- 306, total: 1
UPDATE `clients` SET `dbirth` = '1959-09-22' WHERE `id` = 306 LIMIT 1;
-- Updated: 0


-- Task 2

SELECT `id` FROM `clients` WHERE `dbirth` < '1933-01-01';
-- 94, 109, 136, 147, 217, 301, 365, 416, 420, 441, 540, 782, 950, total: 13
UPDATE `clients` SET `phone` = NULL WHERE `id` IN (94, 109, 136, 147, 217, 301, 365, 416, 420, 441, 540, 782, 950) LIMIT 13;
-- Updated: 13

-- Task 3

SELECT `id` FROM `clients` WHERE `name` = 'John' and `lastname` = 'Ohara';
-- 570, total: 1
UPDATE `clients` SET `name` = 'Johanna', `gender` = 'F' WHERE `id` = 570 LIMIT 1;
-- Updated: 1


-- Task 4

SELECT `id` FROM `clients` WHERE `name` = 'Humberto' and `lastname` = 'Hoosier';
-- 673, total: 1
UPDATE `clients` SET `phone` = '5557777' WHERE `id` = 673 LIMIT 1;
-- Updated: 1

SELECT `id` FROM `clients` WHERE `name` = 'Irene' and `lastname` = 'Schreiber';
-- 447, total: 1
UPDATE `clients` SET `phone` = '1112222' WHERE `id` = 447 LIMIT 1;
-- Updated: 1

SELECT `id` FROM `clients` WHERE `name` = 'Donna' and `lastname` = 'Wallace';
-- 293, total: 1
UPDATE `clients` SET `phone` = '9993333' WHERE `id` = 293 LIMIT 1;
-- Updated: 1


-- Task 5

DELETE FROM `clients` WHERE `id` IN (215, 340, 449, 470, 607) LIMIT 5;
-- Deleted: 4


-- Task 6

SELECT `id` FROM `clients` WHERE `gender` = 'M' AND ((`dbirth` >= '1941-01-01' AND `dbirth` <= '1941-04-30') OR (`dbirth` >= '1972.09.10' AND `dbirth` <= '1972.09.15'));
-- 383, 839, total: 2
DELETE FROM `clients` WHERE `id` IN (383, 839) LIMIT 2;
-- Deleted: 2
