SET GLOBAL log_bin_trust_function_creators = 1;

-- Task 1
DROP FUNCTION IF EXISTS is_jubilee;

DELIMITER //
CREATE FUNCTION is_jubilee(dob DATE) RETURNS INTEGER
BEGIN
    DECLARE age INT;
    SET age = YEAR(NOW()) -  YEAR(dob);
    IF dob IS NULL THEN
        RETURN NULL;
    END IF;
    IF MOD(age, 10) = 0 THEN
        RETURN age;
    ELSE
        RETURN NULL;
    END IF;
END//
DELIMITER ;

SELECT `id`, `birth_date` FROM `salesmen` WHERE is_jubilee(`birth_date`) IS NOT NULL LIMIT 10;

-- Task 2
DROP FUNCTION IF EXISTS reformat_name;

DELIMITER //
CREATE FUNCTION reformat_name(first_name VARCHAR(255), last_name VARCHAR(255), middle_name VARCHAR(255)) RETURNS VARCHAR(255)
BEGIN
    DECLARE fn, mn VARCHAR(255);
    IF first_name IS NULL OR last_name IS NULL OR middle_name IS NULL THEN
        RETURN "######";
    ELSEIF LENGTH(first_name) = 0 OR LENGTH(last_name) = 0 OR LENGTH(middle_name) = 0 THEN
        RETURN "######";
    ELSE
        SET fn = CONCAT(SUBSTR(first_name, 1, 1), '.');
        SET mn = CONCAT(SUBSTR(middle_name, 1, 1), '.');
        RETURN CONCAT_WS(' ', last_name, fn, mn);
    END IF;
END//
DELIMITER ;

SELECT `id`, reformat_name(`first_name`, `last_name`, `middle_name`) FROM `salesmen`  LIMIT 10;

-- Task 3
DROP FUNCTION IF EXISTS salesman_income;

DELIMITER //
CREATE FUNCTION salesman_income(rate DECIMAL(10, 2), income DECIMAL(10, 2)) RETURNS DECIMAL(10, 2)
BEGIN
    IF rate IS NULL OR income IS NULL THEN
        RETURN  NULL;
    END IF;
    RETURN rate * income;
END//
DELIMITER ;

SELECT sm.id, reformat_name(sm.first_name, sm.last_name, sm.middle_name), s.id,
       salesman_income(sm.rate, s.income) FROM `salesmen` AS `sm`
LEFT JOIN `sales` s on sm.id = s.salesman_id LIMIT 10;

-- Task 4
DROP FUNCTION IF EXISTS company_income;

DELIMITER //
CREATE FUNCTION company_income(cost DECIMAL(10, 2), qty INT) RETURNS DECIMAL(20, 2)
BEGIN
    IF cost IS NULL OR qty IS NULL THEN
        RETURN NULL;
    END IF;
    RETURN cost * qty;
END //
DELIMITER ;

SELECT `id`, company_income(`cost`, `qty`) FROM `sales` LIMIT 10;

-- Task 5
DROP PROCEDURE IF EXISTS salesmen_jubilee;

DELIMITER //
CREATE PROCEDURE salesmen_jubilee()
BEGIN
    SELECT `id`, `birth_date`, is_jubilee(`birth_date`) AS `age`  FROM `salesmen` WHERE is_jubilee(`birth_date`) IS NOT NULL;
END //
DELIMITER ;

CALL salesmen_jubilee();

-- Task 6
DROP PROCEDURE IF EXISTS get_product_by_id;

DELIMITER //
CREATE PROCEDURE get_product_by_id(IN id INT UNSIGNED)
BEGIN
    IF id IS NULL THEN
        SELECT NULL;
    END IF ;
    SELECT p.name, g.name, p.plu, p.cost, p.available FROM `products` AS p
          LEFT JOIN `groups` AS g ON p.group_id = g.id
          WHERE `group_id` = id;
END //
DELIMITER ;

CALL get_product_by_id(3);

-- Task 7
DROP PROCEDURE IF EXISTS get_sales_list_by_product;

DELIMITER //
CREATE PROCEDURE get_sales_list_by_product(IN product VARCHAR(255), IN days INT UNSIGNED)
BEGIN
    DECLARE product_id INT UNSIGNED;
    DECLARE date_start DATE;
    SET product_id = (SELECT `id` FROM `products` WHERE `name` = product LIMIT 1);
    IF product IS NULL or days IS NULL THEN
        SELECT NULL;
    END IF ;
    IF product_id IS NULL THEN
        SELECT NULL;
    ELSE
        SET date_start = NOW() - INTERVAL days + 1 DAY;
        SELECT `date`, `cost`, `qty`, `income`, reformat_name(s.first_name, s.last_name, s.middle_name) AS `sales_man` FROM `sales`
            LEFT JOIN salesmen s on s.id = sales.salesman_id
            WHERE `date` >= date_start AND `date` <= NOW() AND `product_id` = product_id;
    END IF ;
END //
DELIMITER ;

CALL get_sales_list_by_product('Пила Ryobi RTS-1800G', 200);

-- Task 8

DROP PROCEDURE IF EXISTS get_incorrect_price;

DELIMITER  //
CREATE PROCEDURE get_incorrect_price()
BEGIN
    DECLARE line INT;
    SET line = (SELECT COUNT(*) FROM `sales` AS `s`
    LEFT JOIN `products` AS p ON p.id = s.product_id
    WHERE s.cost != p.cost AND (p.cost_changed_at IS NULL OR s.date > p.cost_changed_at) LIMIT 1);
    IF line = 0 THEN
        SELECT "All prices are ok";
    ELSE
        SELECT s.id, s.date, s.product_id, s.cost, p.cost FROM `sales` AS `s`
        LEFT JOIN `products` AS p ON p.id = s.product_id
        WHERE s.cost != p.cost AND (p.cost_changed_at IS NULL OR s.date > p.cost_changed_at);
    END IF ;

END //
DELIMITER ;

CALL get_incorrect_price();