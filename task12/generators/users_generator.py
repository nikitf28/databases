import datetime
import random

phones = []

first_names = []
second_names = []
middle_names = []

f = open('dictionaries/first_names.txt')
for line in f:
    first_names.append(line[:-1])
f.close()

f = open('dictionaries/second_names.txt')
for line in f:
    if line[-2] == 'А' or line[-2] == 'Я':
        continue
    second_names.append(line[0] + line[1:-1].lower())
f.close()

f = open('dictionaries/middle_names.csv')
for line in f:
    middle_names.append(line.split(',')[0])
f.close()


def generate_phone():
    global phones
    while True:
        phone = '+79'
        for i in range(9):
            phone += str(random.randint(0, 9))
        if phone not in phones:
            break
    return phone


def generate_dates_array(number):
    dates = []
    begin = 1546300800
    end = 1640995200
    for i in range(number):
        dates.append(random.randint(begin, end))
    dates.sort()
    str_dates = []
    for date in dates:
        str_dates.append(datetime.datetime.utcfromtimestamp(date).strftime('%Y-%m-%d %H-%M-%S'))
    return str_dates


n = 10000
str_dates = generate_dates_array(10000)

query = ''

for i in range(n):
    first_name = random.choice(first_names)
    second_name = random.choice(second_names)
    middle_name = random.choice(middle_names)
    phone = generate_phone()
    registration_date = str_dates[i]
    line = "('%s', '%s', '%s', '%s', '%s'),\n" % (first_name, second_name, middle_name, phone, registration_date)
    query += line

f = open('user.txt', 'w')
f.write(query)
f.close()
