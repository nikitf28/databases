import datetime
import random

parkings = [('Шестой корпус ИГУ', 52.25045, 104.26378, 10),
            ('Библиотека ИГУ', 52.25069, 104.26062, 10),
            ('Пятёрочка Союз', 52.25309, 104.26010, 5),
            ('Юрия Тена 12', 52.25341, 104.25817, 5),
            ('Общежитие 3 ИГУ', 52.25089, 104.25360, 5),
            ('Общежитие 10 ИГУ', 52.25326, 104.25052, 5),
            ('ТЦ Снегирь', 52.25387, 104.24864, 10),
            ('Остановка Университетский', 52.25552, 104.25444, 5),
            ('Остановка Юрия Тена', 52.25464, 104.25708, 5),
            ('Остановка ИГУ', 52.24941, 104.26653, 5),
            ('ИСЗФ', 52.24854, 104.26393, 10),
            ('Библиотека им. Молчанова', 52.25129, 104.26726, 10),
            ('ИРНИТУ', 52.26253, 104.26110, 1),
            ('ТЦ Яркомолл', 52.26847, 104.29016, 30),
            ('ДС Труд', 52.27757, 104.28409, 15),
            ('ИМЭИ', 52.27501, 104.27956, 10),
            ('Остров Конный', 52.26978, 104.27966, 10),
            ('Александр III', 52.27535, 104.27704, 20),
            ('Сквер им. Кирова', 52.28770, 104.28077, 20)]


def generate_dates(n):
    ready = 0
    dates_b = []
    dates_e = []
    begin = 1647302400
    end = 1653955200
    while ready < n:
        for i in range(246):
            start = random.randint(begin + 4000, begin + 86400)
            dates_b.append(start)
        begin += 86400
        ready += 246
        if begin > end:
            break
    dates_b.sort()
    for date in dates_b:
        dates_e.append(date + random.randint(120, 3600))

    return dates_b, dates_e


n = 10000

dates_b, dates_e = generate_dates(n)

query = ''

for i in range(n):
    user_id = random.randint(1, 10000)
    scooter_id = i % 246 + 1
    started_date = datetime.datetime.utcfromtimestamp(dates_b[i])
    finished_date = datetime.datetime.utcfromtimestamp(dates_e[i])
    parking_b = random.choice(parkings)
    parking_e = random.choice(parkings)
    started_lat = parking_b[1]
    started_lon = parking_b[2]
    finished_lat = parking_e[1]
    finished_lon = parking_e[2]
    rate_id = 3
    cost = (dates_e[i] - dates_b[i]) // 60 * 4
    if started_date.weekday() >= 5:
        rate_id = 4
        cost = (dates_e[i] - dates_b[i]) // 60 * 6

    line = "(%s, %s, '%s', '%s', %s, %s, %s, %s, %s, %s),\n" \
           % (user_id, scooter_id, started_date.strftime('%Y-%m-%d %H-%M-%S'), finished_date.strftime('%Y-%m-%d %H-%M-%S'),
              started_lat, started_lon, finished_lat, finished_lon, rate_id, cost)
    query += line

f = open('trips.txt', 'w')
f.write(query)
f.close()