import random

query = 'INSERT INTO `scooters`(`plate`, `SN`, `model_id`, `status_id`, `lat`, `lon`, `battery_charge`) VALUES'
plate_num = 1000
SNs = []

parkings = [('Шестой корпус ИГУ', 52.25045, 104.26378, 10),
            ('Библиотека ИГУ', 52.25069, 104.26062, 10),
            ('Пятёрочка Союз', 52.25309, 104.26010, 5),
            ('Юрия Тена 12', 52.25341, 104.25817, 5),
            ('Общежитие 3 ИГУ', 52.25089, 104.25360, 5),
            ('Общежитие 10 ИГУ', 52.25326, 104.25052, 5),
            ('ТЦ Снегирь', 52.25387, 104.24864, 10),
            ('Остановка Университетский', 52.25552, 104.25444, 5),
            ('Остановка Юрия Тена', 52.25464, 104.25708, 5),
            ('Остановка ИГУ', 52.24941, 104.26653, 5),
            ('ИСЗФ', 52.24854, 104.26393, 10),
            ('Библиотека им. Молчанова', 52.25129, 104.26726, 10),
            ('ИРНИТУ', 52.26253, 104.26110, 1),
            ('ТЦ Яркомолл', 52.26847, 104.29016, 30),
            ('ДС Труд', 52.27757, 104.28409, 15),
            ('ИМЭИ', 52.27501, 104.27956, 10),
            ('Остров Конный', 52.26978, 104.27966, 10),
            ('Александр III', 52.27535, 104.27704, 20),
            ('Сквер им. Кирова', 52.28770, 104.28077, 20)]


def generate_SN():
    SN = ''
    alphabet = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']
    while len(SN) < 16:
        SN += random.choice(alphabet)
    if SN in SNs:
        return generate_SN()
    else:
        SNs.append(SN)
        return SN


def generate_scooter(line):
    global plate_num
    plate = 'IR' + str(plate_num)
    plate_num += 1
    SN = generate_SN()
    model_id = random.randint(1, 7)
    status_id = 1
    status_chance = random.random()
    if 0.8 <= status_chance < 0.9:
        status_id = 3
    if 0.9 <= status_chance < 1:
        status_id = 2
    battery_charge = random.randint(10, 100)
    lat = round(line[1] + ((random.randint(0, 19) - 9.5) / 100000), 5)
    lon = round(line[2] + ((random.randint(0, 19) - 9.5) / 100000), 5)
    line = "('%s', '%s', %s, %s, %s, %s, %s), \n" % (plate, SN, model_id, status_id, lat, lon, battery_charge)
    return line


for line in parkings:
    scooters_num = random.randint(0, 20)
    for i in range(scooters_num):
        query += generate_scooter(line)

print(query)
