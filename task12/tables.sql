DROP TABLE IF EXISTS `models`;

CREATE TABLE `models`(
    `id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `model` VARCHAR(255) NOT NULL,
    `battery_capacity` INT UNSIGNED NOT NULL,
    `max_distance` TINYINT UNSIGNED NOT NULL,
    `max_speed` TINYINT UNSIGNED NOT NULL,
    `deleted` TINYINT UNSIGNED DEFAULT 0,
    PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `parkings`;

-- ------------------------------

CREATE TABLE `parkings`(
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `lat` DECIMAL(7, 5) NOT NULL,
    `lon` DECIMAL(8, 5) NOT NULL,
    `radius` SMALLINT UNSIGNED NOT NULL,
    `deleted` TINYINT UNSIGNED DEFAULT 0,
    PRIMARY KEY (`id`)
);


-- ------------------------------

DROP TABLE IF EXISTS `statuses`;

CREATE TABLE `statuses`(
    `id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(255) NOT NULL,
    `deleted` TINYINT UNSIGNED DEFAULT 0,
    PRIMARY KEY (`id`)
);

-- -------------------------------

DROP TABLE IF EXISTS `scooters`;

CREATE TABLE `scooters`(
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `plate` VARCHAR(255) NOT NULL,
    `SN` VARCHAR(255) NOT NULL,
    `model_id` TINYINT UNSIGNED,
    `status_id` TINYINT UNSIGNED,
    `lat` DECIMAL(7, 5) NOT NULL,
    `lon` DECIMAL(8, 5) NOT NULL,
    `battery_charge` TINYINT UNSIGNED NOT NULL,
    `deleted` TINYINT UNSIGNED DEFAULT 0,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`model_id`) REFERENCES `models`(`id`) ON DELETE SET NULL,
    FOREIGN KEY (`status_id`) REFERENCES `statuses`(`id`) ON DELETE SET NULL
);


-- -----------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users`(
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `first_name` VARCHAR(255) NOT NULL,
    `second_name` VARCHAR(255) NOT NULL,
    `middle_name` VARCHAR(255) NOT NULL,
    `phone` VARCHAR(255) NOT NULL,
    `registration_date` DATETIME NOT NULL,
    `deleted` TINYINT UNSIGNED DEFAULT 0,
    PRIMARY KEY (`id`)
);

-- --------------------------------------

DROP TABLE IF EXISTS `rates`;

CREATE TABLE `rates`(
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `rate` DECIMAL(5, 2) NOT NULL,
    `deleted` TINYINT UNSIGNED DEFAULT 0,
    PRIMARY KEY (`id`)
);

-- ----------------------------------------

DROP TABLE IF EXISTS `trips`;

CREATE TABLE `trips`(
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id` INT UNSIGNED,
    `scooter_id` INT UNSIGNED,
    `started_date` DATETIME NOT NULL,
    `finished_date` DATETIME DEFAULT NULL,
    `started_lat` DECIMAL(7, 5) NOT NULL,
    `started_lon` DECIMAL(8, 5) NOT NULL,
    `finished_lat` DECIMAL(7, 5) DEFAULT NULL,
    `finished_lon` DECIMAL(8, 5) DEFAULT NULL,
    `rate_id` INT UNSIGNED,
    `cost` DECIMAL(6, 2) DEFAULT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE SET NULL,
    FOREIGN KEY (`scooter_id`) REFERENCES `scooters`(`id`) ON DELETE SET NULL,
    FOREIGN KEY (`rate_id`) REFERENCES `rates`(`id`) ON DELETE SET NULL
);

DELIMITER //

DROP TRIGGER IF EXISTS `add_trip`;
CREATE TRIGGER `add_trip` AFTER INSERT ON `trips` FOR EACH ROW
    BEGIN
        UPDATE `scooters` SET `scooters`.`status_id` = 4 WHERE `scooters`.`id` = NEW.scooter_id;
    END//

DROP TRIGGER IF EXISTS `finish_trip`;
CREATE TRIGGER `finish_trip` BEFORE UPDATE ON `trips` FOR EACH ROW
    BEGIN
        IF NOT OLD.finished_date <=> NEW.finished_date THEN
            UPDATE `scooters` SET `scooters`.`status_id` = 1 WHERE `scooters`.`id` = OLD.scooter_id;
        END IF;
    END//
DELIMITER ;
-- ----------------------------------

DROP VIEW IF EXISTS `scooters_info`;

CREATE VIEW `scooters_info` AS
    SELECT sc.plate, sc.SN, m.model, s.title, sc.lat, sc.lon, sc.battery_charge, sc.battery_charge/100*m.max_distance AS `remain_distance` FROM `scooters` AS `sc`
    LEFT JOIN models AS m ON sc.model_id = m.id
    LEFT JOIN statuses AS s ON sc.status_id = s.id;

-- ----------------------------------

DROP VIEW IF EXISTS `top10_users`;

CREATE VIEW `top10_users` AS
    SELECT t.user_id, u.first_name, u.second_name, u.middle_name, u.phone, SUM(t.cost) FROM  `trips` AS `t`
    LEFT JOIN users AS u ON t.user_id = u.id
    GROUP BY t.cost
    ORDER BY t.cost DESC
    LIMIT 10;

-- ------------------------------

DROP VIEW IF EXISTS `unoccupied_scooters`;

CREATE VIEW `unoccupied_scooters` AS
    SELECT * FROM `scooters_info` WHERE title = 'Готов к аренде';