-- Task 1
SELECT COUNT(*)
FROM `clients`;

SELECT COUNT(*)
FROM `clients`
WHERE YEAR(`dbirth`) < 1990;

SELECT COUNT(`name`)
FROM `clients`
WHERE `name` = 'Thomas';

SELECT COUNT(`name`)
FROM `clients`
WHERE `name` = 'Barbara';

SELECT COUNT(`name`)
FROM `clients`
WHERE `name` = 'Willie';


-- Task 2
SELECT YEAR(`dbirth`) as 'year', COUNT(*) as 'amount'
FROM `clients`
GROUP BY YEAR(`dbirth`)
ORDER BY YEAR(`dbirth`);

SELECT YEAR(`dbirth`) as 'year', `gender`, COUNT(*) as 'amount'
FROM `clients`
GROUP BY YEAR(`dbirth`), `gender`
ORDER BY YEAR(`dbirth`);


-- Task 3
SELECT MONTHNAME(`dbirth`) as 'month', COUNT(*) as 'amount'
FROM `clients`
GROUP BY MONTHNAME(`dbirth`), MONTH(`dbirth`)
ORDER BY MONTH(`dbirth`);


-- Task 4
SELECT AVG(TIMESTAMPDIFF(YEAR, `dbirth`, NOW())) as 'average age'
FROM `clients`;

SELECT MIN(TIMESTAMPDIFF(YEAR, `dbirth`, NOW())) as 'average age'
FROM `clients`;

SELECT MAX(TIMESTAMPDIFF(YEAR, `dbirth`, NOW())) as 'average age'
FROM `clients`;


-- Task 5
SELECT `name`
FROM `clients`
WHERE YEAR(`dbirth`) >= 1960 AND YEAR(`dbirth`) <= 1969
GROUP BY `name`
ORDER BY `name`;

SELECT `name`
FROM `clients`
WHERE YEAR(`dbirth`) >= 1970 AND YEAR(`dbirth`) <= 1979
GROUP BY `name`
ORDER BY `name`;

SELECT `name`
FROM `clients`
WHERE YEAR(`dbirth`) >= 1980 AND YEAR(`dbirth`) <= 1989
GROUP BY `name`
ORDER BY `name`;

SELECT `name`
FROM `clients`
WHERE YEAR(`dbirth`) >= 1990 AND YEAR(`dbirth`) <= 1999
GROUP BY `name`
ORDER BY `name`;


-- Task 6
SELECT gender, COUNT(*) as 'amount'
FROM `clients`
WHERE YEAR(`dbirth`) >= 1940 AND YEAR(`dbirth`) <= 1949
GROUP BY `gender`;

SELECT gender, COUNT(*) as 'amount'
FROM `clients`
WHERE YEAR(`dbirth`) >= 1950 AND YEAR(`dbirth`) <= 1959
GROUP BY `gender`;

SELECT gender, COUNT(*) as 'amount'
FROM `clients`
WHERE YEAR(`dbirth`) >= 1960 AND YEAR(`dbirth`) <= 1969
GROUP BY `gender`;

SELECT gender, COUNT(*) as 'amount'
FROM `clients`
WHERE YEAR(`dbirth`) >= 1970 AND YEAR(`dbirth`) <= 1979
GROUP BY `gender`;