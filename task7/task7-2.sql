-- Task 2
SELECT `date`, SUM(`hits`) as 'hits', SUM(`loads`) as 'loads'
FROM `stats`
GROUP BY `date`
ORDER BY `date`;

SELECT `country`, SUM(`hits`) as 'hits', SUM(`loads`) as 'loads'
FROM `stats`
GROUP BY `country`
ORDER BY `country`;

SELECT `date`, `country`, SUM(`hits`) as 'hits', SUM(`loads`) as 'loads'
FROM `stats`
GROUP BY `date`, `country`
ORDER BY `date`, `country`;

-- Task 3
SELECT `date`, AVG(`hits`) as 'hits'
FROM `stats`
GROUP BY `date`
ORDER BY `date`;

SELECT `country`, AVG(`hits`) as 'hits'
FROM `stats`
GROUP BY `country`
ORDER BY `country`;

-- Task 4
SELECT `date`, AVG(`loads`) as 'loads'
FROM `stats`
GROUP BY `date`
ORDER BY `date`;

SELECT `country`, AVG(`loads`) as 'loads'
FROM `stats`
GROUP BY `country`
ORDER BY `country`;

-- Task 5
SELECT `date`, MIN(`loads`) as 'loads'
FROM `stats`
GROUP BY `date`;

SELECT `date`, MAX(`loads`) as 'loads'
FROM `stats`
GROUP BY `date`;

-- Task 6
SELECT `date`, SUM(`loads`) / SUM(`hits`) as 'convertion'
FROM `stats`
GROUP BY `date`;

-- Task 7
SELECT `date`, SUM(`loads`) / SUM(`hits`) as 'convertion'
FROM `stats`
GROUP BY `date`
ORDER BY `convertion` DESC
LIMIT 1;

-- Task 8
SELECT `country`, SUM(`loads`) / SUM(`hits`) as 'convertion'
FROM `stats`
GROUP BY `country`
ORDER BY `convertion` DESC
LIMIT 5;