import pymysql


def connect():
    db = pymysql.connect(
        host='localhost',
        user='nikita',
        password='super618',
        db='homework'
    )
    return db


def num_of_lines():
    db = connect()
    cur = db.cursor()
    cur.execute("SELECT COUNT(*) FROM `subject_schedules_time`;")
    return cur.fetchone()[0]


print(num_of_lines())
