CREATE TABLE `Films`
(
    `id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `title` VARCHAR(255),
    `director` VARCHAR(255),
    `rating` DECIMAL(4, 3),
    `year` YEAR,
    `min_age` TINYINT UNSIGNED
);

INSERT INTO `Films`(`title`, `director`, `rating`, `year`, `min_age`)
VALUES(
    'Властелин колец: Возвращение короля',
    'Питер Джексон',
    8.633,
    2003,
    12
);


INSERT INTO `Films`(`title`, `director`, `rating`, `year`, `min_age`)
VALUES(
    'Форрест Гамп',
    'Роберт Земекис',
    8.909,
    1994,
    16
);


INSERT INTO `Films`(`title`, `director`, `rating`, `year`, `min_age`)
VALUES(
    'Утиные истории',
    'Боб Хэчкок',
    7.896,
    1987,
    0
);


INSERT INTO `Films`(`title`, `director`, `rating`, `year`, `min_age`)
VALUES(
    'Утиные истории (перезапуск)',
    'Мэттью Хэмфрис',
    8.213,
    2017,
    6
);


INSERT INTO `Films`(`title`, `director`, `rating`, `year`, `min_age`)
VALUES(
    'Черный Плащ',
    'Тэд Стоунс',
    7.639,
    1991,
    6
);
