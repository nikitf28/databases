-- Task 1
SELECT `id`, YEAR(`dbirth`) as `birth_year`
FROM `clients`
LIMIT 10;


-- Task 2
SELECT `id`, TIMESTAMPDIFF(YEAR, `dbirth`, NOW()) as `age`
FROM `clients`
LIMIT 10;

SELECT `id`, TIMESTAMPDIFF(YEAR, `dbirth`, CONCAT(YEAR(NOW()), DATE_FORMAT(NOW(), '-12-31'))) as `age`
FROM `clients`
LIMIT 10;


-- Task 3
SELECT `id`, TIMESTAMPDIFF(YEAR, `dbirth`, '1992-12-08') as `age`
FROM `clients`
WHERE TIMESTAMPDIFF(YEAR, `dbirth`, '1992-12-08') >= 0
LIMIT 10;

SELECT `id`, TIMESTAMPDIFF(YEAR, `dbirth`, '2024-07-15') as `age`
FROM `clients`
LIMIT 10;


-- Task 4
SELECT `id`, `dbirth`
FROM `clients`
WHERE DATE_FORMAT(`dbirth`, '%m-%d') BETWEEN DATE_FORMAT(NOW(), '%m-%d') AND DATE_FORMAT(NOW() + INTERVAL 7 DAY, '%m-%d')
LIMIT 10;

SELECT `id`, `dbirth`
FROM `clients`
WHERE DATE_FORMAT(`dbirth`, '%m-%d') BETWEEN DATE_FORMAT(NOW(), '%m-%d') AND DATE_FORMAT(NOW() + INTERVAL 14 DAY, '%m-%d')
LIMIT 10;

SELECT `id`, `dbirth`
FROM `clients`
WHERE DATE_FORMAT(`dbirth`, '%m-%d') BETWEEN DATE_FORMAT(NOW(), '%m-%d') AND DATE_FORMAT(LAST_DAY(NOW()), '%m-%d')
LIMIT 10;