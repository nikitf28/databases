-- MySQL dump 10.13  Distrib 8.0.28, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: homework
-- ------------------------------------------------------
-- Server version	8.0.28-0ubuntu0.20.04.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `students_4`
--

DROP TABLE IF EXISTS `students_4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `students_4` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `speciality_id` int unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `speciality_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_4`
--

LOCK TABLES `students_4` WRITE;
/*!40000 ALTER TABLE `students_4` DISABLE KEYS */;
INSERT INTO `students_4` VALUES (1,1,'Иванов Сергей Петрович','Разработка ПО'),(2,1,'Сергеев Пётр Васильевич','Разработка ПО'),(3,1,'Иванов Иванов Иванович','Разработка ПО'),(4,1,'Коршунов Дмитрий Сергеевич','Разработка ПО'),(5,2,'Петров Василий Васильевич','Дизайн'),(6,2,'Воронов Дмитрий Васильевич','Дизайн'),(7,2,'Зорошов Кирилл Дмитриевич','Дизайн'),(8,3,'Марков Дмитрий Иванович','Data Science'),(9,3,'Громов Пётр Аркадьевич','Data Science'),(10,4,'Зелёнов Аркадий Сергеевич','Game Dev'),(11,4,'Антонов Антон Антонович','Game Dev');
/*!40000 ALTER TABLE `students_4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students_4_nf`
--

DROP TABLE IF EXISTS `students_4_nf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `students_4_nf` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `speciality_id` int unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `speciality_id` (`speciality_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_4_nf`
--

LOCK TABLES `students_4_nf` WRITE;
/*!40000 ALTER TABLE `students_4_nf` DISABLE KEYS */;
INSERT INTO `students_4_nf` VALUES (1,1,'Иванов Сергей Петрович'),(2,1,'Сергеев Пётр Васильевич'),(3,1,'Иванов Иванов Иванович'),(4,1,'Коршунов Дмитрий Сергеевич'),(5,2,'Петров Василий Васильевич'),(6,2,'Воронов Дмитрий Васильевич'),(7,2,'Зорошов Кирилл Дмитриевич'),(8,3,'Марков Дмитрий Иванович'),(9,3,'Громов Пётр Аркадьевич'),(10,4,'Зелёнов Аркадий Сергеевич'),(11,4,'Антонов Антон Антонович');
/*!40000 ALTER TABLE `students_4_nf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specialities_4_nf`
--

DROP TABLE IF EXISTS `specialities_4_nf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `specialities_4_nf` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `speciality_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specialities_4_nf`
--

LOCK TABLES `specialities_4_nf` WRITE;
/*!40000 ALTER TABLE `specialities_4_nf` DISABLE KEYS */;
INSERT INTO `specialities_4_nf` VALUES (1,'Разработка ПО'),(2,'Дизайн'),(3,'Data Science'),(4,'Game Dev');
/*!40000 ALTER TABLE `specialities_4_nf` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-09  1:49:49
