-- Variant 22

CREATE TABLE `students_4` (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `speciality_id` INT(10) UNSIGNED NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `speciality_name` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
);

INSERT INTO students_4 (id, speciality_id, name, speciality_name)
VALUES(1, 1, 'Иванов Сергей Петрович', 'Разработка ПО'),
(2, 1, 'Сергеев Пётр Васильевич', 'Разработка ПО'),
(3, 1, 'Иванов Иванов Иванович', 'Разработка ПО'),
(4, 1, 'Коршунов Дмитрий Сергеевич', 'Разработка ПО'),
(5, 2, 'Петров Василий Васильевич', 'Дизайн'),
(6, 2, 'Воронов Дмитрий Васильевич', 'Дизайн'),
(7, 2, 'Зорошов Кирилл Дмитриевич', 'Дизайн'),
(8, 3, 'Марков Дмитрий Иванович', 'Data Science'),
(9, 3, 'Громов Пётр Аркадьевич', 'Data Science'),
(10, 4, 'Зелёнов Аркадий Сергеевич', 'Game Dev');

-- Normal Form

CREATE TABLE `students_4_nf` (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `speciality_id` INT(10) UNSIGNED NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `speciality_id` (`speciality_id`)
);

CREATE TABLE `specialities_4_nf` (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `speciality_name` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
);

INSERT INTO `specialities_4_nf`(`speciality_name`)
VALUES ('Разработка ПО'), ('Дизайн'), ('Data Science'), ('Game Dev');

INSERT INTO `students_4_nf`(`speciality_id`, `name`)
VALUES(1, 'Иванов Сергей Петрович'),
    (1, 'Сергеев Пётр Васильевич'),
    (1, 'Иванов Иванов Иванович'),
    (1, 'Коршунов Дмитрий Сергеевич'),
    (2, 'Петров Василий Васильевич'),
    (2, 'Воронов Дмитрий Васильевич'),
    (2, 'Зорошов Кирилл Дмитриевич'),
    (3, 'Марков Дмитрий Иванович'),
    (3, 'Громов Пётр Аркадьевич'),
    (4, 'Зелёнов Аркадий Сергеевич'),
    (4, 'Антонов Антон Антонович');