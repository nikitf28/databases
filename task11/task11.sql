-- Task 1

DROP TABLE IF EXISTS `price_change_journal`;

CREATE TABLE `price_change_journal`(
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `product_id` INT UNSIGNED NOT NULL,
    `date_of_change` DATE NOT NULL,
    `old_price` DECIMAL(10, 2) NOT NULL,
    `new_price` DECIMAL(10, 2) NOT NULL,
    PRIMARY KEY (`id`)
);



DELIMITER //

DROP TRIGGER IF EXISTS `price_change`;

CREATE TRIGGER `price_change` AFTER UPDATE ON `products` FOR EACH ROW
    IF NEW.cost != OLD.cost THEN
        INSERT INTO `price_change_journal`(`product_id`, `date_of_change`, `old_price`, `new_price`)
            VALUES (NEW.id, NOW(), OLD.cost, NEW.cost);
    END IF //

DELIMITER ;


-- Task 2

DELIMITER //

DROP TRIGGER IF EXISTS `delete_group`;

CREATE TRIGGER `delete_group` AFTER DELETE ON `groups` FOR EACH ROW
    UPDATE `products` SET group_id = NULL, available = 0 WHERE `group_id` = OLD.id;

DELIMITER ;